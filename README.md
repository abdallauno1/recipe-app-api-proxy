# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' -Port to listen on (degault: '8080')
* 'APP_HOST' - Hostanme of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forword requests to (default: '9000')
